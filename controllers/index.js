'use strict';

// REQUIREMENTS
const auth = require('../middlewares/auth');
const user = require('../models/user');

const express = require('express');
const router = express.Router();

const FB = require('fb');

router.get('/', (req, res) => {
  FB.options({
    "appId": global.nconf.get('FB_APPID'),
    "appSecret": global.nconf.get('FB_SECRET'),
    "redirectUri": global.nconf.get('FB_REDIRECT_URL')
  });
  if (req.session.access_token) {
    user.get(req.session.access_token, (err, userData) => {
      if (err) return res.status(500).render('error', { err: err });
      res.render('index', { user: userData });
    })
  } else {
    res.render('homepage', {
      fbUrl: FB.getLoginUrl({
        scope: global.nconf.get('FB_SCOPE')
      })
    });
  }
});

router.get('/login/callback', auth);

module.exports = router;