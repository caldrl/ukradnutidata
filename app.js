'use strict';

// REQUIREMENTS
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const session = require('express-session');
const path = require('path');

// CONFIG
global.nconf = require('nconf');
global.nconf
  .argv()
  .env()
  .defaults({
    'PORT': 1337
  });

// PUG TEMPLATE ENGINE INIT
app.set('view engine', 'pug');

// SESSION INIT
app.use(session({
  secret: 'byCaldr BIAČ!!!',
  cookie: { maxAge: 10000 }
}));

// BODYPARSER INIT
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// CONTROLLERS INIT
app.use(require('./controllers'));

// PUBLIC/STATIC INIT
app.use('/public', express.static('./public'));

// WEB SERVER START!
app.listen(nconf.get('PORT'), () => {
  console.log(`Listening on port ${nconf.get('PORT')}`);
});