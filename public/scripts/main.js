$(function () {
    $('body')
        .on('click', '[data-fb-href]', function () {
            $this = $(this);
            $('.logo-wrapper').addClass('animated infinite tada')
            setTimeout(function () {
                window.location.href = $this.attr('data-fb-href');
            }, 1000)
        });

    $('#user-json').JSONView($('#user-json').text());
})