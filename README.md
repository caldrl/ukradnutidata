# #ukradnuTiData
![logo](http://i.imgur.com/q4Ns5vT.png)

## Setup
```bash
$ #clone repository
$ cd ./ukradnutidata
$ npm install
$ node app.js
```
## Enviroment
You have to set few enviroment variables to configure application.
```json
{
    "NODE_ENV": "production",
    "PORT": 3000,
    "FB_APPID": "---xxx---",
    "FB_SECRET": "---xxx---",
    "FB_REDIRECT_URL": "http://localhost:3000/login/callback",
    "FB_SCOPE": "public_profile,user_friends,email,user_about_me,user_birthday,user_education_history,user_events,user_games_activity,user_hometown,user_likes,user_location,user_managed_groups,user_photos,user_posts,user_relationships,user_relationship_details,user_religion_politics,user_tagged_places,user_videos,user_website,user_work_history,read_custom_friendlists,read_insights,read_audience_network_insights,read_page_mailboxes,manage_pages,publish_pages,publish_actions,rsvp_event,pages_show_list,pages_manage_cta,pages_manage_instant_articles,ads_read,ads_management,business_management,pages_messaging,pages_messaging_phone_number",
    "FB_FIELDS": "id,name,about,context,age_range,bio,birthday,cover,currency,devices,education,email,favorite_athletes,favorite_teams,first_name,gender,hometown,inspirational_people,install_type,installed,interested_in,is_shared_login,is_verified,languages,last_name,link,locale,location,middle_name,name_format,relationship_status,religion,security_settings,meeting_for,payment_pricepoints,political,quotes,shared_login_upgrade_required_by,significant_other,sports,test_group,third_party_id,timezone,updated_time,verified,video_upload_limits,viewer_can_send_gift,website,work,accounts{about,affiliation,awards},achievements{application,data,end_time,message},admined_groups{administrator,description,email,cover,icon,name,owner,link},albums{cover_photo,description,event},feed,photos{from,created_time,album,link,likes}",
    "FB_POST_MESSAGE": "Haha!!! #ukradnuTiData. Made with ❤ byCaldr!",
    "FB_POST_URL": "http://localhost:3000/",
    "FB_OG_IMAGE": "http://localhost:3000/public/images/fb-og.png",
    "FB_OG_TITLE": "#UkradnuTiData",
    "FB_OG_URL": "http://localhost:3000/",
    "FB_OG_SITENAME": "UkradnuTiData",
    "FB_OG_TYPE": "website"
}
```