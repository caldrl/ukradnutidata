const FB = require('fb');
const Step = require('step');

module.exports = (req, res, next) => {
    var code = req.query.code;

    if (req.query.error) {
        if (err) return res.status(500).render({ err: err })
    } else if (!code) {
        return res.redirect('/');
    }

    Step(
        function exchangeCodeForAccessToken() {
            FB.napi('oauth/access_token', {
                client_id: FB.options('appId'),
                client_secret: FB.options('appSecret'),
                redirect_uri: FB.options('redirectUri'),
                code: code
            }, this);
        },
        function extendAccessToken(err, result) {
            if (err) res.status(500).render('error', { status: 500 })

            FB.napi('oauth/access_token', {
                client_id: FB.options('appId'),
                client_secret: FB.options('appSecret'),
                grant_type: 'fb_exchange_token',
                fb_exchange_token: result.access_token
            }, this);
        },
        function (err, result) {
            if (err) return res.status(500).render({ err: err })

            req.session.access_token = result.access_token;
            req.session.expires = result.expires || 0;
            return res.redirect('/');
        }
    );
}