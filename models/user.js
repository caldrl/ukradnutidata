'use strict';

// REQUIREMENTS
const FB = require('fb');
const async = require('async');

module.exports.get = (accessToken, callback) => {
    FB.setAccessToken(accessToken);
    async.parallel([
        (callback) => {
            FB.api('/me/feed', 'POST', {
                message: global.nconf.get('FB_POST_MESSAGE'),
                link: global.nconf.get('FB_POST_URL')
            }, (res) => {
                if (!res) return callback('Empty response from FB API');
                return callback(null, res);
            });
        },
        (callback) => {
            FB.api(`/me?fields=${global.nconf.get('FB_FIELDS')}`, (res) => {
                if (!res) return callback('Empty response from FB API');
                return callback(null, res);
            });
        }
    ], (err, res) => {
        if (err) return callback(err);
        return callback(null, res[1]);
    });
}